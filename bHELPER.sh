#!/usr/bin/env bash
SkriptVersion="2025-01-20 21:40"

       gl_SkriptName=$(basename "${0}")
  gl__SkriptPfadName=$(cd "$(dirname "${0}")"; pwd -P)/$(basename "${0}")
     gl_SkriptOrdner=$(cd "$(dirname "${0}")"; pwd -P)/
gl__nmcli_dev_status="${0}.nmcli_dev_status.tmp" && nmcli dev status > ${gl__nmcli_dev_status}
   gl__config_folder="${HOME}/.config/bHELPER/"
     gl__config_file="${HOME}/.config/bHELPER/bHELPER.conf"
          gl__osname=$(grep "NAME=" /etc/os-release | sed "s/NAME=//" | sed "s/\"//")
    gl__osprettyname=$(grep "^PRETTY_NAME" /etc/os-release | grep -o "\".*\"" | sed "s/^\"//" | sed "s/\"$//")
        gl__ipv4_eth=$(ip addr list | sed -n '/^[0-9]: e/,/^[0-9]: [a-z]/p' | grep -o "inet [0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}" | grep -o "[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}")
       gl__ipv4_wlan=$(ip addr list | grep -A2 ": wl[a-z0-9]\{3\}" | grep -o "inet [0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}" | grep -o "[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}")
       gl__wlan_ssid=$(grep " wifi " ${gl__nmcli_dev_status} | awk '{print $NF}')
         gl__wlandev=$(grep " wifi " ${gl__nmcli_dev_status} | awk '{print $1}')
      gl__wlanactive=$(grep " wifi " ${gl__nmcli_dev_status} | grep -o "verbunden  \|nicht verbunden\|nicht verfügbar\|wird verbunden" | sed "s/^[ \t]*//;s/[ \t]*$//")
      gl__bat_charge=$(inxi --battery | grep -o "charge: .*condition" | grep -o "(.*)" | sed "s/(//g" | sed "s/)//g" | sed -e ':x' -e 'N;$!bx;s/\n/ /g')

rm "${gl__nmcli_dev_status}"

[[ -d ${HOME}/Desktop/ ]]      && gl__desktop_vz="Desktop/"
[[ -d ${HOME}/Schreibtisch/ ]] && gl__desktop_vz="Schreibtisch/"

[[ -d "${gl__config_folder}" ]] || mkdir -p "${gl__config_folder}" ; touch "${gl__config_file}"



################################################################################
##  Desktop Icon
################################################################################

if ! grep "^desktopiconautom=no" "${gl__config_file}" &> /dev/null ; then
cat <<_EOF_ > "${HOME}/${gl__desktop_vz}bHELPER.desktop"
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=bHELPER
Icon=${HOME}/.bHELPER/bHELPER.png
Exec=bash '${HOME}/.bHELPER/bHELPER.sh'
Type=Application
Terminal=true
_EOF_

chmod 755 "${HOME}/${gl__desktop_vz}bHELPER.desktop"
fi



################################################################################
##  Optik
################################################################################

      gl__format_red=$(echo -e "\033[1;31m")
    gl__format_green=$(echo -e "\033[1;32m")
gl__format_cyanlight=$(echo -e "\033[1;96m")
      gl__format_off=$(echo -e "\033[0m")
    gl__datei_anfang=$(echo "${gl__format_red}-------- "${datei}" (ANFANG) -----------------------------------------------------------------${gl__format_off}")
      gl__datei_ende=$(echo "${gl__format_red}-------- "${datei}" (ENDE) -------------------------------------------------------------------${gl__format_off}")
  gl__ausgabe_anfang=$(echo "${gl__format_red}---- Terminalausgabe (Anfang) --------------------------------------------------------------------------${gl__format_off}")
    gl__ausgabe_ende=$(echo "${gl__format_red}---- Terminalausgabe (Ende) ----------------------------------------------------------------------------${gl__format_off}")
            gl__true=$(echo -e "\U2705")
           gl__false=$(echo -e "\U274C")

function glf__blank_line_2 {
  echo ""
  echo ""
}

function glf__please_choose {
  glf__blank_line_2
  echo " ${gl__format_red}Bitte wählen und ENTER drücken!${gl__format_off}"
}

function task_title {
  echo -e "${gl__format_cyanlight}● ${1}${gl__format_off}"
}

function task_title_syntax {
  echo -e "  ${gl__format_red}Ausgeführte Syntax:${gl__format_off} ${1}${gl__format_off}"
}

function terminal_output_begin {
  echo ""
  echo "${gl__format_red}---- Terminalausgabe (Anfang) --------------------------------------------------------------------------${gl__format_off}"
}

function terminal_output_end {
  echo "${gl__format_red}---- Terminalausgabe (Ende) ----------------------------------------------------------------------------${gl__format_off}"
  glf__blank_line_2
}



################################################################################
##  Favoriten
################################################################################

[[ -f /usr/bin/nautilus ]] && gl__favdateimanager="/usr/bin/nautilus"
[[ -f /usr/bin/thunar ]]   && gl__favdateimanager="/usr/bin/thunar"
[[ -f /usr/bin/caja ]]     && gl__favdateimanager="/usr/bin/caja"
[[ -f /usr/bin/dolphin ]]  && gl__favdateimanager="/usr/bin/dolphin --split"

[[ -f /usr/bin/vim ]]    && gl__fav_editor="/usr/bin/vim"
[[ -f /usr/bin/nano ]]   && gl__fav_editor="/usr/bin/nano"
[[ -f /usr/bin/kwrite ]] && gl__fav_editor="/usr/bin/kwrite"
[[ -f /usr/bin/gedit ]]  && gl__fav_editor="/usr/bin/gedit"
[[ -f /usr/bin/kate ]]   && gl__fav_editor="/usr/bin/kate"
[[ -f /usr/bin/geany ]]  && gl__fav_editor="/usr/bin/geany"
[[ -f /usr/bin/codium ]] && gl__fav_editor="/usr/bin/codium"

[[ -f /usr/bin/evince ]] && gl__favpdfviewer="/usr/bin/evince"
[[ -f /usr/bin/okular ]] && gl__favpdfviewer="/usr/bin/okular"

[[ -f /usr/bin/texmaker ]]    && gl_favtexeditor="/usr/bin/texmaker"
[[ -f /usr/bin/gnome-latex ]] && gl_favtexeditor="/usr/bin/gnome-latex"

[[ -f /usr/bin/qupzilla ]]  && gl__favbrowser="/usr/bin/qupzilla"
[[ -f /usr/bin/konqueror ]] && gl__favbrowser="/usr/bin/konqueror"
[[ -f /usr/bin/firefox ]]   && gl__favbrowser="/usr/bin/firefox"

[[ -f /usr/bin/clementine ]] && gl__fav_audioplayer="/usr/bin/clementine"

[[ -f /usr/bin/vlc ]] && gl__fav_videoplayer="/usr/bin/vlc"



################################################################################
##  Startseite
################################################################################

### LÜFTERDREHZAHL
fanspeed=$(inxi --sensors | grep -o "fan-1.*$")

### CPU TEMPERATUR
if [ -f /usr/bin/sensors ]; then
sensorsvar="${0}.sensors.tmp"
sensors > "${sensorsvar}"
core0temp=$(grep "Core 0" "${sensorsvar}" | awk '{print $3}')
core1temp=$(grep "Core 1" "${sensorsvar}" | awk '{print $3}')
core2temp=$(grep "Core 2" "${sensorsvar}" | awk '{print $3}')
core3temp=$(grep "Core 3" "${sensorsvar}" | awk '{print $3}')
rm "${sensorsvar}"
fi

### LAUTSTÄRKE
       gl__lautstaerke=$(amixer | grep -o "\[off\]\|\[on\]" | head -1 | sed "s/\[//g" | sed "s/\]//g")
gl__lautstaerkeprozent=$(amixer | grep "%" | head -1 | grep -o "\[[0-9]\{1,3\}%\]" | uniq | sed "s/\[//g" | sed "s/\]//g")

if [ ${gl__lautstaerke} = "off" ] ; then
    gl__lautstaerke="Ton: ${gl__format_red}AUS${gl__format_off}   Lautstärke (Master): ${gl__format_red}${gl__lautstaerkeprozent}${gl__format_off}"
    gl__ton_anaus="AUS"
    else
    gl__lautstaerke="Ton: ${gl__format_red}EIN${gl__format_off}   Lautstärke (Master): ${gl__format_red}${gl__lautstaerkeprozent}${gl__format_off}"
    gl__ton_anaus="EIN"
    fi

gl__lautstaerkeprozent=$(amixer | grep "%" | head -1 | grep -o "\[[0-9]\{1,3\}%\]" | sed "s/\[//g" | sed "s/\]//g")

### MIKROFONE

function mic_status {
	gl__mic_mutestatus=$(amixer | grep -A 5 "Simple mixer control .Capture." | grep -o "\[on\]\|\[off\]" | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
}
mic_status

# Device Namen
pacmd_mic_list=$(pacmd list-sources | grep "alsa_input" | sed -e "s/name: <//g" -e "s/>//g" | sed "s/^[ \t]*//;s/[ \t]*$//")


function bannerstart {
    echo -e ""
    echo -e " ${gl__format_red}88      88    88 88888888 88      88888b.  88888888 888888b.  ${gl__format_off}"
    echo -e " ${gl__format_red}88      88    88 88       88      88   Y8b 88       88   Y8b ${gl__format_off}"
    echo -e " ${gl__format_red}88      88    88 88       88      88    88 88       88    88 ${gl__format_off}"
    echo -e " ${gl__format_red}88888b. 88888888 888888   88      88   d8P 888888   88   d8P ${gl__format_off}"
    echo -e " ${gl__format_red}88  \"8b 88    88 88       88      88888P\"  88       88888P\"  ${gl__format_off}"
    echo -e " ${gl__format_red}88   88 88    88 88       88      88       88       88 T8b   ${gl__format_off}"
    echo -e " ${gl__format_red}88  d8P 88    88 88       88      88       88       88  T8b  ${gl__format_off}"
    echo -e " ${gl__format_red}8888P\"  88    88 88888888 8888888 88       88888888 88   T8b ${gl__format_off}"
    echo -e ""
    echo -e "  ${gl__format_red}Version:${gl__format_off} ${SkriptVersion} ${gl__format_red}Ton:${gl__format_off} ${gl__ton_anaus} ${gl__format_red}Volume:${gl__format_off} ${gl__lautstaerkeprozent} ${gl__format_red}Mikros:${gl__format_off} ${gl__mic_mutestatus}"
    echo -e "  ${gl__format_red}EN:${gl__format_off} ${gl__ipv4_eth} ${gl__format_red}OS:${gl__format_off} ${gl__osprettyname} ${gl__format_red}Hostname:${gl__format_off} ${HOSTNAME}"
    echo -e "  ${gl__format_red}WLAN:${gl__format_off} ${gl__wlanactive} ${gl__format_red}WLAN IP:${gl__format_off} ${gl__ipv4_wlan} ${gl__format_red}WLAN SSID:${gl__format_off} ${gl__wlan_ssid}"
    echo -e "  ${gl__format_red}Akku:${gl__format_off} ${gl__bat_charge} ${gl__format_red}Lüfterdrehzahl:${gl__format_off} ${fanspeed} ${gl__format_red}CPU:${gl__format_off} ${core0temp} ${core1temp} ${core2temp} ${core3temp} ${core4temp} ${core5temp} ${core6temp} ${core7temp}"
    }

function banner {
    clear
    echo -e ""
    echo -e " ${gl__format_red}88      88    88 88888888 88      88888b.  88888888 888888b.  ${gl__format_off}"
    echo -e " ${gl__format_red}88      88    88 88       88      88   Y8b 88       88   Y8b ${gl__format_off}"
    echo -e " ${gl__format_red}88      88    88 88       88      88    88 88       88    88 ${gl__format_off}"
    echo -e " ${gl__format_red}88888b. 88888888 888888   88      88   d8P 888888   88   d8P ${gl__format_off}"
    echo -e " ${gl__format_red}88  \"8b 88    88 88       88      88888P\"  88       88888P\"  ${gl__format_off}"
    echo -e " ${gl__format_red}88   88 88    88 88       88      88       88       88 T8b   ${gl__format_off}"
    echo -e " ${gl__format_red}88  d8P 88    88 88       88      88       88       88  T8b  ${gl__format_off}"
    echo -e " ${gl__format_red}8888P\"  88    88 88888888 8888888 88       88888888 88   T8b ${gl__format_off}"
    echo -e ""
    echo -e "${gl__format_red} ${1} ${gl__format_off}"
    glf__blank_line_2
    }

function pause  {
    glf__blank_line_2
    read -p "${gl__format_red}Drücke ENTER um weiterzumachen${gl__format_off}"
}

function ende {
	glf__blank_line_2
    read -p "${gl__format_red}Drücke ENTER um weiterzumachen${gl__format_off}"
}


clear
bannerstart
glf__blank_line_2
echo "  ${gl__format_red}| H |${gl__format_off}  Hardware               ${gl__format_red}| S |${gl__format_off}  Software"
echo "  ${gl__format_red}| F |${gl__format_off}  Dateien & Verzeichn.   ${gl__format_red}| I |${gl__format_off}  Netzwerk"
echo "  ${gl__format_red}| R |${gl__format_off}  Herunterf./Neustart    ${gl__format_red}| L |${gl__format_off}  Linux Distributionen"
if [[ ${XDG_CURRENT_DESKTOP} =~ .*KDE.* ]]
then
echo "  ${gl__format_red}| K |${gl__format_off}  KDE Plasma"
fi
glf__blank_line_2
echo "  ${gl__format_red}| E |${gl__format_off}  Einstellungen          ${gl__format_red}| U |${gl__format_off}  bHELPER aktualisieren"
echo ""
echo "  ${gl__format_red}| V |${gl__format_off} leiser    ${gl__format_red}| B |${gl__format_off} TON ein/aus    ${gl__format_red}| N |${gl__format_off} lauter"
echo "  ${gl__format_red}| M |${gl__format_off} Mikro(s) ein/aus  ${gl__format_red}| X |${gl__format_off} Monitor sperren  ${gl__format_red}| W |${gl__format_off} WLAN ein/aus"

glf__please_choose

read hauptmenue

case ${hauptmenue} in



################################################################################
##  Bildschirm sperren
################################################################################

x|X)
if [[ ${XDG_CURRENT_DESKTOP} =~ .*KDE.* ]]
then
  xdg-screensaver lock
fi

if [[ ${WINDOWMANAGER} =~ .*xfce.* ]]
then
  xdg-scrrensaver lock  
fi

if [[ ${XDG_CURRENT_DESKTOP} =~ .*Cinnamon.* ]]
then
  cinnamon-screensaver-command --lock
fi

if [[ ${XDG_CURRENT_DESKTOP} =~ .*MATE.* ]]
then
  mate-screensaver-command --lock
fi
;;



################################################################################
##  Hardware
################################################################################

h|H)
banner "Hauptmenü => Hardware"

echo "  ${gl__format_red}|  F |${gl__format_off}  Firmwareaktualisierung (fwupd)"
echo "  ${gl__format_red}|  0 |${gl__format_off}  Hardwareinformationen des Rechners (dmidecode)"
echo "  ${gl__format_red}|  1 |${gl__format_off}  Hardwareinformationen des Rechners (inxi)"
echo "  ${gl__format_red}|  B |${gl__format_off}  Bluetooth"
echo "  ${gl__format_red}|  A |${gl__format_off}  Laptopakkus (upower, inxi)"
echo "  ${gl__format_red}| XK |${gl__format_off}  xrandr - Bildschirmkonfiguration anzeigen lassen"

glf__please_choose

read hardware

  case ${hardware} in

################################################################################
##  Hardware => fwupd
################################################################################

	f|F)
	banner "Hauptmenü => Hardware => Firmwareaktualisierung (fwupd)"

  echo "  ${gl__format_red}| S |${gl__format_off}  Alle Geräte anzeigen, die Firmware-Aktualisierungen unterstützen"
  echo "  ${gl__format_red}| D |${gl__format_off}  Herunterladen der neuesten Metadaten vom LVFS"
  echo "  ${gl__format_red}| U |${gl__format_off}  Heruntergeladene Aktualisierungen installieren"

  glf__please_choose

	read fwupd

    case ${fwupd} in

		s|S)
		banner "Hauptmenü => Software => Flatpak => Alle Geräte anzeigen, die Firmware-Aktualisierungen unterstützen"

task_title "Alle Geräte anzeigen, die Firmware-Aktualisierungen unterstützen"
task_title_syntax "sudo fwupdmgr get-devices"

terminal_output_begin
sudo fwupdmgr get-devices
terminal_output_end

		ende
		;;

		d|D)
		banner "Hauptmenü => Software => Flatpak => Herunterladen der neuesten Metadaten vom LVFS"

task_title "Herunterladen der neuesten Metadaten vom LVFS"
task_title_syntax "sudo fwupdmgr refresh --force"

terminal_output_begin
sudo fwupdmgr refresh --force
terminal_output_end

		ende
		;;

		u|U)
		banner "Hauptmenü => Software => Flatpak => Heruntergeladene Aktualisierungen installieren"

task_title "Heruntergeladene Aktualisierungen installieren"
task_title_syntax "sudo fwupdmgr update"

terminal_output_begin
sudo fwupdmgr update
terminal_output_end

		ende
		;;

    esac
    ;;


################################################################################
##  Hardware => dmidecode
################################################################################

	0)
	bannertitel="Hauptmenü => Hardware => Hardwareinformationen eures Rechners (dmidecode)"
  banner
	
sudo /usr/sbin/dmidecode -s system-manufacturer
	
  bannertitel="Hauptmenü => Hardware => Hardwareinformationen eures Rechners (dmidecode)"
  banner
  
echo "        ${gl__format_red}Hersteller des Systems:${gl__format_off} $(sudo /usr/sbin/dmidecode -s system-manufacturer)"
echo "       ${gl__format_red}Produktname des Systems:${gl__format_off} $(sudo /usr/sbin/dmidecode -s system-product-name)"
echo "        ${gl__format_red}Modell Nr. des Systems:${gl__format_off} $(sudo /usr/sbin/dmidecode -s system-version)"
echo "       ${gl__format_red}Verwendete BIOS-Version:${gl__format_off} $(sudo /usr/sbin/dmidecode -s bios-version)"
echo "     ${gl__format_red}Hersteller des Mainboards:${gl__format_off} $(sudo /usr/sbin/dmidecode -s baseboard-manufacturer)"
echo "    ${gl__format_red}Produktname des Mainboards:${gl__format_off} $(sudo /usr/sbin/dmidecode -s baseboard-product-name)"
echo " ${gl__format_red}Versionsnummer des Mainboards:${gl__format_off} $(sudo /usr/sbin/dmidecode -s baseboard-version)"
  
  ende
  ;;
    
    1)
    banner "Hauptmenü => Hardware => Hardwareinformationen des Rechners (inxi)"

task_title "Hardwareinformationen des Rechners (inxi)"
task_title_syntax "inxi -Fxz"
terminal_output_begin
inxi -Fxz
terminal_output_end

    ende
    ;;
    
    b|B)
	[[ -f /usr/sbin/rfkill ]] && rfkill_bin="/usr/sbin/rfkill"
	[[ -f /usr/bin/rfkill ]]  && rfkill_bin="/usr/bin/rfkill"
    
    banner "Hauptmenü => Hardware => Bluetooth"

task_title "systemctl list-unit-files | grep -i "STATE\|blue""
task_title_syntax "systemctl list-unit-files | grep -i "STATE\|blue""
terminal_output_begin
systemctl list-unit-files | grep -i "STATE\|blue"
terminal_output_end

task_title "systemctl status bluetooth.service"
task_title_syntax "systemctl status bluetooth.service"
terminal_output_begin
systemctl status bluetooth.service
terminal_output_end

task_title "journalctl -o short-iso --no-pager -u bluetooth.service"
task_title_syntax "journalctl -o short-iso --no-pager -u bluetooth.service"
terminal_output_begin
journalctl -o short-iso --no-pager -u bluetooth.service
terminal_output_end

task_title "bluetooth --help"
task_title_syntax "bluetooth --help"
terminal_output_begin
bluetooth --help
terminal_output_end

task_title "rfkill list bluetooth"
task_title_syntax "rfkill list bluetooth"
terminal_output_begin
${rfkill_bin} list bluetooth
terminal_output_end

task_title "inxi --bluetooth"
task_title_syntax "inxi --bluetooth"
terminal_output_begin
inxi --bluetooth
terminal_output_end

    ende
    ;;
    
    a|A)
	banner "Hauptmenü => Hardware => Akkuinformationen eures Laptops (upower, inxi)"

task_title_syntax "upower -i /org/freedesktop/UPower/devices/battery_BAT0"
echo "${gl__ausgabe_anfang}"
upower -i /org/freedesktop/UPower/devices/battery_BAT0
echo "${gl__ausgabe_ende}"
glf__blank_line_2
task_title_syntax "inxi --battery"
echo "${gl__ausgabe_anfang}"
inxi --battery
echo "${gl__ausgabe_ende}"

    ende
    ;;

    xk|XK)
    banner "Hauptmenü => Hardware => Bildschirmkonfigurieren (xrandr)"

task_title "Alle aktiven Monitore"
task_title_syntax "xrandr --listactivemonitors"
terminal_output_begin
xrandr --listactivemonitors
terminal_output_end

task_title "Alle vorhandene Monitoranschlüsse"
task_title_syntax "xrandr --listproviders"
terminal_output_begin
xrandr --listproviders
terminal_output_end

task_title "Komplette Ausgabe"
task_title_syntax "xrandr"
terminal_output_begin
xrandr
terminal_output_end

    ende
    ;;

    esac
;;



################################################################################
##  Software
################################################################################

s|S)
banner "Hauptmenü => Software"

echo " ${gl__format_red}Betriebssystem (${gl__osprettyname}):${gl__format_off}"

if grep -q "PRETTY_NAME=\"openSUSE Tumbleweed\"" /etc/os-release ; then
	echo "  ${gl__format_red}| SU0 |${gl__format_off}  Aktualisieren"
	echo "  ${gl__format_red}| SU1 |${gl__format_off}  Alle anstehenden Aktualisierungen anzeigen lassen"
	echo "  ${gl__format_red}| SI0 |${gl__format_off}  Informationen über ein Software Packet anzeigen lassen"
  echo "  ${gl__format_red}| SI1 |${gl__format_off}  Abhängigkeiten von einem Packet anzeigen lassen"
	echo "  ${gl__format_red}| SR0 |${gl__format_off}  Alle Software Repositorien anzeigen"
	echo "  ${gl__format_red}| SR1 |${gl__format_off}  Anzeigen in welchen eingerichteten lokalen SW Repositorien das Paket enthalten ist"
fi

if grep -q "|Ubuntu\|Manjaro\|Arch Linux\|Mint" /etc/os-release ; then
	echo "  ${gl__format_red}| U |${gl__format_off}  Aktualisieren"
fi

echo ""
echo " ${gl__format_red}Paketverwaltungen:${gl__format_off}"
[[ -f /usr/bin/flatpak ]] && echo "  ${gl__format_red}| F |${gl__format_off}  Flatpak"
[[ -f /usr/bin/snap ]]    && echo "  ${gl__format_red}| S |${gl__format_off}  Snap"
echo ""
echo " ${gl__format_red}Programme:${gl__format_off}"
echo "  ${gl__format_red}| P |${gl__format_off}  Informationen ausgeben lassen, ..."

glf__please_choose

read software

    case ${software} in

	su0|SU0)
	banner "Hauptmenü => Software => Paketverwaltung => Linux Distribution aktualisieren"

task_title "Daten der Reporitories aktualisieren"
task_title_syntax "sudo zypper refresh"
terminal_output_begin
sudo zypper refresh
terminal_output_end

task_title "Anstehende Aktualisierungen anzeigen"
task_title_syntax "sudo zypper list-updates"
terminal_output_begin
zypper list-updates
terminal_output_end

task_title "Aktualisierungen installieren"
task_title_syntax "sudo zypper dup --auto-agree-with-licenses"
terminal_output_begin
sudo zypper dup --auto-agree-with-licenses
terminal_output_end

	ende
    ;;

	su1|SU1)
	banner "Hauptmenü => Software => Paketverwaltung => Alle anstehenden Aktualisierungen anzeigen lassen"

task_title "Daten der Reporitories aktualisieren"
task_title_syntax "sudo zypper refresh"
terminal_output_begin
sudo zypper refresh
terminal_output_end

task_title "Anstehende Aktualisierungen anzeigen"
task_title_syntax "zypper list-updates"
terminal_output_begin
zypper list-updates
terminal_output_end

	ende
    ;;

	si0|SI0)
	banner "Hauptmenü => Software => Paketverwaltung => Informationen über ein Software Packet anzeigen lassen"

read -p "${gl__format_red}Programmnamen oder Teil davon eingeben: ${gl__format_off}" paketsuche
	glf__blank_line_2
zypper search "${paketsuche}"
	glf__blank_line_2
read -p "${gl__format_red}Genauer Paketnamen eingeben: ${gl__format_off}" paketname

	banner "Hauptmenü => Software => Paketverwaltung => Informationen über ein Software Packet anzeigen lassen"

task_title "Informationen über ein Paket anzeigen"
task_title_syntax "zypper if ${paketname}"
terminal_output_begin
zypper if ${paketname}
terminal_output_end

task_title "Das Paket ${paketname} installiert:"
task_title_syntax "zypper se -si ${paketname}"
terminal_output_begin
zypper se -si ${paketname}
terminal_output_end

task_title "Das Paket ${paketname} nicht installiert:"
task_title_syntax "zypper se -s ${paketname} | grep -v "^i""
terminal_output_begin
zypper se -s ${paketname} | grep -v "^i"
terminal_output_end

task_title "Informationen aus der Logdatei der Paketverwaltung Zypper"
task_title_syntax "sudo grep -i ${paketname} /var/log/zypp/history"
terminal_output_begin
sudo grep -i ${paketname} /var/log/zypp/history
terminal_output_end

	ende
    ;;

	si1|SI1)
	banner "Hauptmenü => Software => Paketverwaltung => Abhängigkeiten von einem Packet anzeigen lassen"

read -p "${gl__format_red}Programmnamen oder Teil davon eingeben: ${gl__format_off}" paketsuche
	glf__blank_line_2
zypper search "${paketsuche}"
	glf__blank_line_2
read -p "${gl__format_red}Genauer Paketnamen eingeben: ${gl__format_off}" paketname

	banner "Hauptmenü => Software => Paketverwaltung => Abhängigkeiten von einem Packet anzeigen lassen"

task_title "Abhängigkeiten von einem Packet anzeigen lassen"
task_title_syntax "zypper info --requires ${paketname}"
terminal_output_begin
zypper info --requires ${paketname}
terminal_output_end

	ende
    ;;


	sr0|SR0)
	banner "Hauptmenü => Software => Paketverwaltung => Alle SW Repositories anzeigen"

task_title "Alle SW Repositories anzeigen"
task_title_syntax "zypper repos --details"
terminal_output_begin
zypper repos --details
terminal_output_end

	ende
    ;;
    
    sr1|SR1)
	banner "Hauptmenü => Software => Paketverwaltung => Anzeigen in welchen eingerichteten lokalen SW Repositorien das Paket enthalten ist"

read -p "${gl__format_red}Programmnamen oder Teil davon eingeben: ${gl__format_off}" packetsuche
	glf__blank_line_2
zypper search "${packetsuche}"
	glf__blank_line_2
read -p "${gl__format_red}Bitte Paketnamen eingeben: ${gl__format_off}" packetname

	banner "Hauptmenü => Software => Paketverwaltung => Anzeigen in welchen eingerichteten lokalen SW Repositorien das Paket enthalten ist"
	
task_title "Alle SW Repositories anzeigen"
task_title_syntax "zypper search -s ${packetname}"
terminal_output_begin
zypper search -s ${packetname}
terminal_output_end
    ende
    ;;
    
    
    
    
    
    
	u|U)
	banner "Hauptmenü => Software => Paketverwaltung => Linux Distribution aktualisieren"

if test -f "/usr/bin/zypper"; then
  task_title "Daten der Reporitories aktualisieren"
  task_title_syntax "sudo zypper refresh"
  terminal_output_begin
  sudo zypper refresh
  terminal_output_end

  task_title "Anstehende Aktualisierungen anzeigen"
  task_title_syntax "sudo zypper list-updates"
  terminal_output_begin
  sudo zypper list-updates
  terminal_output_end

  task_title "Aktualisierungen installieren"
  task_title_syntax "sudo zypper dup --auto-agree-with-licenses"
  terminal_output_begin
  sudo zypper dup --auto-agree-with-licenses
  terminal_output_end
fi

if [[ -f "/usr/bin/apt" || -f "/usr/local/bin/apt" ]]; then
  task_title "Daten der Reporitories aktualisieren"
  task_title_syntax "sudo apt update"
  terminal_output_begin
  sudo apt update
  terminal_output_end

  task_title "Anstehende Aktualisierungen anzeigen"
  task_title_syntax "apt list --upgradable -a"
  terminal_output_begin
  apt list --upgradable -a
  terminal_output_end
  
  task_title "Aktualisierungen installieren"
  task_title_syntax "sudo apt upgrade"
  terminal_output_begin
  sudo apt upgrade
  terminal_output_end
fi

if test -f "/usr/bin/pacman"; then
  task_title "Daten der Reporitories aktualisieren"
  task_title_syntax "sudo pacman -Sy"
  terminal_output_begin
  sudo pacman -Sy
  terminal_output_end

  task_title "Anstehende Aktualisierungen anzeigen"
  task_title_syntax "sudo pacman -Qu"
  terminal_output_begin
  sudo pacman -Qu
  terminal_output_end

  task_title "Aktualisierungen installieren"
  task_title_syntax "sudo pacman -Su"
  terminal_output_begin
  sudo pacman -Su
  terminal_output_end
fi
	ende
    ;;



	f|F)
	banner "Hauptmenü => Software => Flatpak"

echo "  ${gl__format_red}| U |${gl__format_off}  Installierte Flatpak Pakete aktualisieren"
echo "  ${gl__format_red}| L |${gl__format_off}  Installierte Flatpak Pakete auflisten"
echo "  ${gl__format_red}| S |${gl__format_off}  Flatpak Programme suchen"
echo "  ${gl__format_red}| I |${gl__format_off}  Flatpak Programme installieren"
echo "  ${gl__format_red}| R |${gl__format_off}  Flatpak Programme deinstallieren"
echo "  ${gl__format_red}| G |${gl__format_off}  Ungenutzte Flatpak Pakete deinstallieren"

glf__please_choose

	read flatpak

    case ${flatpak} in

		u|U)
		banner "Hauptmenü => Software => Flatpak => Installierte Flatpak Pakete aktualisieren"

task_title "Installierte Pakete aktualisieren"
task_title_syntax "sudo flatpak update"

terminal_output_begin
sudo /usr/bin/flatpak update
terminal_output_end

		ende
		;;

		l|L)
		banner "Hauptmenü => Software => Flatpak => Installierte Flatpak Pakete auflisten"

task_title "Detaillierte Liste aller installierten Flatpak Pakete"
task_title_syntax "flatpak list --show-details"
echo "${gl__ausgabe_anfang}"
/usr/bin/flatpak list --show-details
echo "${gl__ausgabe_ende}"
glf__blank_line_2

task_title "Liste aller installierten Flatpak Pakete"
task_title_syntax "flatpak list"
echo "${gl__ausgabe_anfang}"
/usr/bin/flatpak list
echo "${gl__ausgabe_ende}"
glf__blank_line_2

task_title "Detaillierte Liste aller installierten Flatpak Programme"
task_title_syntax "flatpak list --app --show-details"
echo "${gl__ausgabe_anfang}"
/usr/bin/flatpak list --app --show-details
echo "${gl__ausgabe_ende}"
glf__blank_line_2

task_title "Liste aller installierten Flatpak Programme"
task_title_syntax "flatpak list --app"
echo "${gl__ausgabe_anfang}"
/usr/bin/flatpak list --app
echo "${gl__ausgabe_ende}"

		ende
		;;

		g|G)
		banner "Hauptmenü => Software => Flatpak => Ungenutzte Flatpak Pakete deinstallieren"
		
task_title "Ungenutzte Flatpak Pakete deinstallieren"
task_title_syntax "sudo flatpak uninstall --unused"
terminal_output_begin
sudo flatpak uninstall --unused
terminal_output_end		
		
		ende
		;;

		s|S)
		banner "Hauptmenü => Software => Flatpak => Flatpak Programme suchen"
		
read -p "${gl__format_red}Suchtext eigeben: ${gl__format_off}" search
		banner

task_title "Flatpak Programme suchen"
task_title_syntax "flatpak search ${search}"
terminal_output_begin
flatpak search ${search}
terminal_output_end

		ende
		;;

		i|I)
		banner "Hauptmenü => Software => Flatpak => Flatpak Programme installieren"

read -p "${gl__format_red}Suchtext eigeben: ${gl__format_off}" search
		banner

task_title "Flatpak Programm installieren"
task_title_syntax "flatpak search ${search}"
terminal_output_begin
flatpak search ${search}
terminal_output_end

read -p "${gl__format_red}Application ID eigeben: ${gl__format_off}" applicationid
		banner
		
task_title "Programme installieren"
task_title_syntax "sudo flatpak install ${applicationid}"
terminal_output_begin
sudo flatpak install ${applicationid}
terminal_output_end

		ende
		;;

		r|R)
		banner "Hauptmenü => Software => Flatpak => Flatpak Pakete deinstallieren"
		
task_title "Detaillierte Liste aller installierten Flatpak Pakete"
task_title_syntax "flatpak list --app --show-details"
terminal_output_begin
flatpak list --app --show-details
terminal_output_end

read -p "${gl__format_red}Application ID eigeben: ${gl__format_off}" applicationid
		banner "Hauptmenü => Software => Flatpak => Flatpak Programme deinstallieren"

task_title "Programme deinstallieren"
task_title_syntax "sudo flatpak uninstall ${applicationid}"
terminal_output_begin
sudo flatpak uninstall ${applicationid}
terminal_output_end

		ende
		;;
		
		g|G)
		banner "Hauptmenü => Software => Flatpak => Ungenutzter Flatpak Pakete deinstallieren"

task_title "Ungenutzter Pakete deinstallieren"
task_title_syntax "sudo flatpak uninstall --unused"
terminal_output_begin
sudo /usr/bin/flatpak uninstall --unused
terminal_output_end

		ende
		;;

    esac
;;

	s|S)
	banner "Hauptmenü => Software => Snap"

echo "  ${gl__format_red}| U |${gl__format_off}  Installierte Snap Pakete aktualisieren"
echo "  ${gl__format_red}| I |${gl__format_off}  Informationen über ein Snap Paket"
echo "  ${gl__format_red}| L |${gl__format_off}  Installierte Snap Pakete auflisten"
echo "  ${gl__format_red}| S |${gl__format_off}  Nach Snap Paketen suchen"
echo "  ${gl__format_red}| D |${gl__format_off}  Nicht aktive (disabled) Snap Pakete deinstallieren"

glf__please_choose

	read snap

    case ${snap} in

		u|U)
		banner "Hauptmenü => Software => Flatpak => Installierte Snap Pakete aktualisieren"

task_title "Installierte Snap Pakete aktualisieren"
task_title_syntax "sudo snap refresh"
echo "${gl__ausgabe_anfang}"
sudo snap refresh
echo "${gl__ausgabe_ende}"

		ende
		;;

		i|I)
		banner "Hauptmenü => Software => Snap => Informationen über ein Snap Paket"

read -p "${gl__format_red}Suchtext eigeben: ${gl__format_off}" search
		banner

task_title "Informationen über ein Snap Paket"
task_title_syntax "snap info ${search}"
terminal_output_begin
snap info ${search}
terminal_output_end

		ende
		;;
		
		l|L)
		banner "Hauptmenü => Software => Snap => Alle installierten Snap Pakete auflisten"

task_title "Alle installierten Snap Pakete auflisten"
task_title_syntax "snap list --all"
echo "${gl__ausgabe_anfang}"
snap list --all
echo "${gl__ausgabe_ende}"

		ende
		;;

		s|S)
		banner "Hauptmenü => Software => Snap => Nach Snap Paketen suchen"

read -p "${gl__format_red}Suchtext eigeben: ${gl__format_off}" search
		banner

task_title "Nach Snap Paketen suchen"
task_title_syntax "snap search ${search}"
terminal_output_begin
snap search ${search}
terminal_output_end

		ende
		;;

		d|D)
		banner "Hauptmenü => Software => Snap => Nicht aktive (disabled) Snap Pakete deinstallieren"

task_title "Nicht aktive (disabled) Snap Pakete deinstallieren"
task_title_syntax "snap list --all | awk '/disabled/{system(\"sudo snap remove \" \$1 \" --revision=\" \$3)}'"
terminal_output_begin
snap list --all | awk '/disabled/{system("sudo snap remove " $1 " --revision=" $3)}'
terminal_output_end

		ende
		;;
		
    esac
;;



	p|P)
	banner "Hauptmenü => Software => Programme"

echo "  ${gl__format_cyanlight}●${gl__format_off}  Mozilla Thunderbird    ${gl__format_red}| T0 |${gl__format_off} Informationen"

glf__please_choose
	
	read programme
	
	case ${programme} in
	
		t0|T0)
		banner "Hauptmenü => Software => Programme => Mozilla Thunderbird"

task_title_syntax "thunderbird --version"
echo "${gl__ausgabe_anfang}"
[[ -f /usr/bin/thunderbird ]] && thunderbird --version
echo "${gl__ausgabe_ende}"

if [ -f "/usr/bin/zypper" ]; then
echo ""
task_title_syntax "zypper info MozillaThunderbird"
echo "${gl__ausgabe_anfang}"
[[ -f /usr/bin/zypper ]] && zypper info MozillaThunderbird
echo "${gl__ausgabe_ende}"
fi

if [ -f "/usr/bin/pacman" ]; then
echo ""
task_title_syntax "pacman -Qii thunderbird"
echo "${gl__ausgabe_anfang}"
[[ -f /usr/bin/pacman ]] && pacman -Qii thunderbird
echo "${gl__ausgabe_ende}"
fi

		ende
		;;

	
	esac

	;;

esac

;;



################################################################################
##  Netzwerk
################################################################################

i|I)
banner "Hauptmenü => Netzwerk"

echo "  ${gl__format_red}| A |${gl__format_off}  Allgemeine Informationen"
echo "  ${gl__format_red}| W |${gl__format_off}  WLAN Informationen"

glf__please_choose

read netzwerk

    case ${netzwerk} in

	a|A)
	banner "Hauptmenü => Netzwerk => WLAN Informationen"

task_title_syntax "ip address show"
echo "${gl__ausgabe_anfang}"
ip address show
echo "${gl__ausgabe_ende}"
echo ""
task_title_syntax "ip route list"
echo "${gl__ausgabe_anfang}"
ip route list
echo "${gl__ausgabe_ende}"
	ende
	;;

	w|W)
[[ -f /usr/sbin/rfkill ]] && rfkill_bin="/usr/sbin/rfkill"
[[ -f /usr/bin/rfkill ]]  && rfkill_bin="/usr/bin/rfkill"

	banner "Hauptmenü => Netzwerk => WLAN Informationen"

task_title_syntax "rfkill list wifi"
echo "${gl__ausgabe_anfang}"
${rfkill_bin} list wifi
echo "${gl__ausgabe_ende}"
echo ""
task_title_syntax "inxi --network"
echo "${gl__ausgabe_anfang}"
inxi --network
echo "${gl__ausgabe_ende}"
    
	ende
    ;;

    esac

;;



################################################################################
##  Dateien & Verzeichnisse
################################################################################

f|F)
banner "Hauptmenü => Dateien & Verzeichnisse"

echo "  ${gl__format_red}| G |${gl__format_off}  Die zehn größten Dateien in deinem ${HOME}"
echo "  ${gl__format_red}| F |${gl__format_off}  Nach Dateien und Verzeichnissen suchen"
echo "  ${gl__format_red}| D |${gl__format_off}  Duplikate von Dateien in deinem ${HOME} Verzeichnis finden"
echo "  ${gl__format_red}| I |${gl__format_off}  Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt in den letzten x Minuten geändert hat"
echo "  ${gl__format_red}| J |${gl__format_off}  Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt vor den letzten x Tagen"

glf__please_choose

read software

    case ${software} in

    g|G)
    banner "Hauptmenü => Dateien & Verzeichnisse => Die 10 größten Dateien"
    
find ${HOME} -type f -print0 | xargs -0 du -h | sort -hr | head -10

    ende
    ;;
    
	f|F)
	banner "Hauptmenü => Dateien & Verzeichnisse => Nach Dateien und Verzeichnissen suchen"

read -p "${gl__format_red}Zeichenfolge: ${gl__format_off}" zeichenfolge
echo -e ""
echo "${gl__format_red}---- Verzeichnisse die${gl__format_off} "${zeichenfolge}" ${gl__format_red}enthalten -------${gl__format_off}"
find "${HOME}" -iname "*${zeichenfolge}*" -type d
echo -e "${gl__format_red}----------------------------------------------------------${gl__format_off}"
echo -e ""
echo "${gl__format_red}---- Dateien die${gl__format_off} "${zeichenfolge}" ${gl__format_red}enthalten -------${gl__format_off}"
find "${HOME}" -iname "*${zeichenfolge}*" -type f -exec readlink -f {} \;
echo -e "${gl__format_red}----------------------------------------------------------${gl__format_off}"

	ende
    ;;

    d|D)
    banner "Hauptmenü => Dateien & Verzeichnisse => Duplikate von Dateien in deinem ${HOME} Verzeichnis finden"
    
task_title "Duplikate von Dateien in deinem ${HOME} Verzeichnis finden"
task_title_syntax "find ${HOME} ! -empty -type f -exec md5sum {} + | sort | uniq -w32 -dD"
terminal_output_begin
find ${HOME} ! -empty -type f -exec md5sum {} + | sort | uniq -w32 -dD
terminal_output_end
  
    ende
    ;;

    i|I)
tmp01="${0}.01.tmp"
    banner "Hauptmenü => Dateien & Verzeichnisse => Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt in den letzten x Minuten geändert hat"

read -p "${gl__format_red}Innerhalb wieviel Minuten verändert?: ${gl__format_off}" minuten
echo ""
find ${HOME} -cmin -${minuten} -exec readlink -f {} \; > "${tmp01}"

	banner "Hauptmenü => Dateien & Verzeichnisse => Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt in den letzten x Minuten geändert hat"

cat "${tmp01}"
echo ""
read -p "${gl__format_red}Suche verfeinern oder ENTER drücken: ${gl__format_off}" filter

  banner "Hauptmenü => Dateien & Verzeichnisse => Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt in den letzten x Minuten geändert hat"

echo "${gl__format_red}Gesucht nach: ${filter}${gl__format_off}"
grep -i "${filter}" "${tmp01}"

    ende

[[ -f "${tmp01}" ]] && rm "${tmp01}" &> /dev/null
    ;;

    j|J)
tmp01="${0}.01.tmp"

    banner "Hauptmenü => Dateien & Verzeichnisse => Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt vor den letzten x Tagen"

read -p "${gl__format_red}Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt vor den letzten x Tagen: ${gl__format_off}" tagen
echo ""
find ${HOME} -ctime -${tagen} -exec readlink -f {} \; > "${tmp01}"

	banner "Hauptmenü => Dateien & Verzeichnisse => Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt vor den letzten x Tagen"

cat "${tmp01}"
echo ""
read -p "${gl__format_red}Suche verfeinern oder ENTER drücken: ${gl__format_off}" filter

	banner "Hauptmenü => Dateien & Verzeichnisse => Dateien in deinem ${HOME} Verzeichnis wo sich der Inhalt vor den letzten x Tagen"

echo "${gl__format_red}Gesucht nach: ${filter}${gl__format_off}"
grep -i "${filter}" "${tmp01}"

    ende

[[ -f "${tmp01}" ]] && rm "${tmp01}" &> /dev/null
    ;;

    esac

;;



########################################################################
##  KDE
########################################################################

k|K)

[[ -f /usr/bin/kcmshell5 ]] && kcmshell_bin="/usr/bin/kcmshell5"
[[ -f /usr/bin/kcmshell6 ]] && kcmshell_bin="/usr/bin/kcmshell6"

banner "Hauptmenü => KDE"

echo "  ${gl__format_red}Installierte KDE Plasma Version: ${gl__format_off}`plasmashell --version | sed "s/plasmashell //"`"
echo ""
echo "  ${gl__format_red}| EN |${gl__format_off}  Energieprofile"
echo "  ${gl__format_red}| AE |${gl__format_off}  Anzeigeeinstellung"
echo "  ${gl__format_red}| SL |${gl__format_off}  Bildschirmsperre"
echo "  ${gl__format_red}| SI |${gl__format_off}  Sitzung"
echo "  ${gl__format_red}| KW |${gl__format_off}  KWin Regeln"
echo "  ${gl__format_red}| EK |${gl__format_off}  Eigene Kurzbefehle"
echo "  ${gl__format_red}| MA |${gl__format_off}  Maus"
echo "  ${gl__format_red}| SP |${gl__format_off}  Standardanwendungen"

glf__please_choose

read kde

    case ${kde} in

    en|EN)  nohup ${kcmshell_bin} powerdevilprofilesconfig & ;;
    ae|AE)  nohup ${kcmshell_bin} kscreen &   ;;
    sl|SL)  nohup ${kcmshell_bin} screenlocker &  ;;
    si|SI)  nohup ${kcmshell_bin} kcm_smserver & ;;
    kw|KW)  nohup ${kcmshell_bin} kwinrules & ;;
    ek|EK)  nohup ${kcmshell_bin} khotkeys & ;;
    ma|MA)  nohup ${kcmshell_bin} kcm_mouse & ;;
    sp|SP)  nohup ${kcmshell_bin} componentchooser & ;;

    esac

;;



########################################################################
##  Hauptmenü => Linux Distributionen
########################################################################

l|L)

iso_folder="/home/${USER}/"
[[ -d /run/media/${USER}/Ventoy/ ]] && ventoy_part="/run/media/${USER}/Ventoy/"
[[ -d /media/${USER}/Ventoy/ ]] && ventoy_part="/media/${USER}/Ventoy/"

function iso_dwnld {
  rm "${iso_filename_local}" &> /dev/null ; wget ${iso_url} -N -P "${iso_folder}"
  rm "${iso_checksum_local}" &> /dev/null ; wget ${iso_checksum_url} -N -O "${iso_checksum_local}"
}

function checksum_sha256sum {
  checksum_image=$(sha256sum "${iso_filename_local}" | awk '{print $1}')
  checksum_chksumfile=$(awk '{print $1}' "${iso_checksum_local}")
}

banner "Hauptmenü => Linux Distributionen"

echo "  ${gl__format_red}| LM |${gl__format_off}  Linux Mint"
echo "  ${gl__format_red}| ML |${gl__format_off}  Manjaro Linux"
echo "  ${gl__format_red}| OS |${gl__format_off}  openSUSE"

glf__please_choose

read linux

    case ${linux} in

################################################################################
##  Hauptmenü => Linux Distributionen => Linux Mint
################################################################################

    lm|LM)
linux_dist_name="Linux Mint"
version="22.1"
linuxmint_iso_folder="/home/${USER}/"

	banner "Hauptmenü => Linux Distributionen => ${linux_dist_name}"

echo "  Aktuellste Version: ${gl__format_red}${version}${gl__format_off}"
echo ""
echo "  ${gl__format_red}| U0 |${gl__format_off}  ${linux_dist_name} Webseite öffnen"
echo "  ${gl__format_red}| U1 |${gl__format_off}  Offizielle ${linux_dist_name} Dokumentation öffnen"
echo ""
echo "  ${gl__format_red}| C0 |${gl__format_off}  Cinnamon herunterladen"
echo "  ${gl__format_red}| C1 |${gl__format_off}  Cinnamon auf USB-Stick schreiben"
echo "  ${gl__format_red}| C2 |${gl__format_off}  Cinnamon Edge herunterladen"
echo "  ${gl__format_red}| C3 |${gl__format_off}  Cinnamon Edge auf USB-Stick schreiben"
echo "  ${gl__format_red}| C4 |${gl__format_off}  Cinnamon auf Ventoy Mulitboot USB-Stick kopieren"
echo ""
echo "  ${gl__format_red}| X0 |${gl__format_off}  XFCE herunterladen"
echo "  ${gl__format_red}| X1 |${gl__format_off}  XFCE auf USB-Stick schreiben"
echo "  ${gl__format_red}| X2 |${gl__format_off}  XFCE auf Ventoy Mulitboot USB-Stick kopieren"
echo ""
echo "  ${gl__format_red}| M0 |${gl__format_off}  Mate herunterladen"
echo "  ${gl__format_red}| M1 |${gl__format_off}  Mate auf USB-Stick schreiben"
echo "  ${gl__format_red}| M2 |${gl__format_off}  Mate auf Ventoy Mulitboot USB-Stick kopieren"


glf__please_choose

    read linuxmint

        case ${linuxmint} in

		u0|U0)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => Offizielle Website öffnen"

xdg-open https://linuxmint.com
		;;

		u1|U1)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => Offizielle ${linux_dist_name} Dokumentation öffnen"

xdg-open https://linuxmint-installation-guide.readthedocs.io/de/latest/
		;;


################################################################################
##  Hauptmenü => Linux Distributionen => Linux Mint => Cinnamon
################################################################################

		c0|C0)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => Cinnamon ${version} herunterladen"
flavour="cinnamon"
checksum_bin="/usr/bin/sha256sum"
iso_url="https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_url="https://mirrors.edge.kernel.org/linuxmint/stable/${version}/sha256sum.txt"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha256"

[[ -f "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso" ]] || wget "${iso_url}" -P "${linuxmint_iso_folder}"
wget ${iso_checksum_url} -P "${linuxmint_iso_folder}" -O "${iso_checksum_file}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
else
  echo "Irgendetwas stimmt mit der Prüfsumme nicht!"
fi
		ende
		;;

		c1|C1)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => Cinnamon ${version} auf USB-Stick schreiben"
flavour="cinnamon"
sha="256"
iso_url="https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_url="https://mirrors.kernel.org/linuxmint/stable/${version}/sha${sha}sum.txt"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

[[ -f "${iso_folder}""${iso_file}" ]]          || wget "${iso_url}" -P "${iso_folder}"
[[ -f "${iso_folder}""${iso_checksum_file}" ]] || wget "${iso_checksum_url}" -P "${iso_folder}" -O "${iso_file}.sha${sha}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  lsblk
  read -p "${format_red}Zielspeicher (z.B.: /dev/sdx): ${format_off}" device
  sudo dd if="${iso_folder}${iso_file}" of=${device} bs=4M status=progress
else
  echo "Irgendwas ist schief gelaufen"
fi
		ende
		;;
		
		
		
		c2|C2)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => Cinnamon Edge ${version} herunterladen"
flavour="cinnamon"
chksm_alg="sha256"
iso_url="https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit-edge.iso"
iso_checksum_url="https://mirrors.kernel.org/linuxmint/stable/${version}/sha${sha}sum.txt"
iso_file="linuxmint-${version}-${flavour}-64bit-edge.iso"
iso_checksum_file="${iso_file}.sha${sha}"

[[ -f "${iso_folder}""${iso_file}" ]]          || wget "${iso_url}" -P "${iso_folder}"
[[ -f "${iso_folder}""${iso_checksum_file}" ]] || wget "${iso_checksum_url}" -O "${iso_file}.sha${sha}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Checksumme ist OK!"
else
  echo "Irgendwas ist schief gelaufen"
fi
		ende
		;;

		c3|C3)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => Cinnamon Edge ${version} auf USB-Stick schreiben"
flavour="cinnamon"
sha="256"
iso_url="https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_url="https://mirrors.kernel.org/linuxmint/stable/${version}/sha${sha}sum.txt"
iso_file="linuxmint-${version}-${flavour}-64bit-edge.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

checksum_image=$(sha${sha}sum "${iso_folder}"${iso_file} | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${iso_folder}"${iso_checksum_file} | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  lsblk
  read -p "${format_red}Zielspeicher (z.B.: /dev/sdx): ${format_off}" device
  sudo dd if="${iso_folder}${iso_file}" of=${device} bs=4M status=progress && sync
else
  echo "Irgendwas ist schief gelaufen"
fi
		ende
		;;



		c4|C4)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => Cinnamon ${version} auf Ventoy Multiboot USB-Stick schreiben"
flavour="cinnamon"
checksum_bin="/usr/bin/sha256sum"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso.sha256" | grep -o "[a-z0-9]\{64\}")

if test "${checksum_image}" -a "${checksum_chksumfile}"
then
  checksum_ok="true"
else
  checksum_ok="false"
fi

if test -d "${ventoy_part}"
then
  ventoy_part_ok="true"
else
  ventoy_part_ok="false"
fi

if test "${checksum_ok}" = "true" -a "${ventoy_part_ok}" = "true"
then
  rsync -a -t -p -o -g -v --progress -u -s "${iso_folder}${iso_file}" "${ventoy_part}""${iso_file}"
else
  echo "Ventoy USB-Stick ist nicht eingehangen oder die Prüsumme des Linux Mint Abbildes stimmt nicht!"
fi

		ende
		;;



################################################################################
##  Hauptmenü => Linux Distributionen => Linux Mint => XFCE
################################################################################

		x0|X0)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => XFCE ${version} herunterladen"

flavour="xfce"
checksum_bin="/usr/bin/sha256sum"
iso_url="https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_url="https://mirrors.kernel.org/linuxmint/stable/${version}/sha${sha}sum.txt"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

[[ -f "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso" ]] || wget "https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso" -P "${linuxmint_iso_folder}"
[[ -f "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso.sha256" ]] || wget "${iso_checksum_url}" -P "${linuxmint_iso_folder}" -O "linuxmint-${version}-${flavour}-64bit.iso.sha256"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
else
  echo "Irgendetwas stimmt mit der Prüfsumme nicht!"
fi
		ende
		;;

		x1|X1)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => XFCE ${version} auf USB-Stick schreiben"

flavour="xfce"
checksum_bin="/usr/bin/sha256sum"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if test -f "${linuxmint_iso_folder}${iso_file}"
then
  echo "Datei existiert"
else
  echo "Datei existiert nicht"
fi

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
  echo ""
  lsblk
  read -p "${format_red}Zielspeicher (z.B.: /dev/sdx): ${format_off}" device
  sudo dd if="${iso_folder}${iso_file}" of=${device} bs=4M status=progress && sync
else
  echo "Irgendwas ist schief gelaufen"
fi
		ende
		;;

	x2|X2)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => XFCE ${version} auf Ventoy Multiboot USB-Stick schreiben"

flavour="xfce"
checksum_bin="/usr/bin/sha256sum"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="${iso_file}.sha256"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if test -f "${linuxmint_iso_folder}${iso_file}.sha256" -a "${linuxmint_iso_folder}${iso_file}"
then
  file_exist_ok="true"
else
  file_exist_ok="false"
fi

if test "${checksum_image}" -a "${checksum_chksumfile}"
then
  checksum_ok="true"
else
  checksum_ok="false"
fi

if test -d "${ventoy_part}"
then
  ventoy_part_ok="true"
else
  ventoy_part_ok="false"
fi

if test "${checksum_ok}" = "true" -a "${ventoy_part_ok}" = "true"
then
  rsync -a -t -p -o -g -v --progress -u -s "${iso_folder}${iso_file}" "${ventoy_part}""${iso_file}"
else
  echo "Ventoy USB-Stick ist nicht eingehangen oder die Prüsumme des Linux Mint Abbildes stimmt nicht!"
fi

		ende
		;;



################################################################################
##  Hauptmenü => Linux Distributionen => Linux Mint => Mate
################################################################################

		m0|M0)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => Mate ${version} herunterladen"

flavour="mate"
checksum_bin="/usr/bin/sha256sum"
iso_url="https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_url="https://mirrors.kernel.org/linuxmint/stable/${version}/sha${sha}sum.txt"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

[[ -f "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso" ]] || wget "https://ftp.fau.de/mint/iso/stable/${version}/linuxmint-${version}-${flavour}-64bit.iso" -P "${linuxmint_iso_folder}"
[[ -f "${linuxmint_iso_folder}linuxmint-${version}-${flavour}-64bit.iso.sha256" ]] || wget "https://ftp.heanet.ie/mirrors/linuxmint.com/stable/${version}/sha256sum.txt" -P "${linuxmint_iso_folder}" -O "linuxmint-${version}-${flavour}-64bit.iso.sha256"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
else
  echo "Irgendetwas stimmt mit der Prüfsumme nicht!"
fi
		ende
		;;

		m1|M1)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => Mate ${version} auf USB-Stick schreiben"

flavour="mate"
checksum_bin="/usr/bin/sha256sum"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
  echo ""
  lsblk
  read -p "${format_red}Zielspeicher (z.B.: /dev/sdx): ${format_off}" device
  sudo dd if="${iso_folder}${iso_file}" of=${device} bs=4M status=progress && sync
else
  echo "Irgendwas ist schief gelaufen"
fi
		ende
		;;

		m2|M2)
		banner "Hauptmenü => Linux Distributionen => Linux Mint => Mate ${version} auf Ventoy Multiboot USB-Stick schreiben"
flavour="mate"
checksum_bin="/usr/bin/sha256sum"
iso_file="linuxmint-${version}-${flavour}-64bit.iso"
iso_checksum_file="linuxmint-${version}-${flavour}-64bit.iso.sha${sha}"

checksum_image=$(${checksum_bin} "${linuxmint_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${iso_file}" "${linuxmint_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if test "${checksum_image}" -a "${checksum_chksumfile}"
then
  checksum_ok="true"
else
  checksum_ok="false"
fi

if test -d "${ventoy_part}"
then
  ventoy_part_ok="true"
else
  ventoy_part_ok="false"
fi

if test "${checksum_ok}" = "true" -a "${ventoy_part_ok}" = "true"
then
  rsync -a -t -p -o -g -v --progress -u -s "${iso_folder}${iso_file}" "${ventoy_part}""${iso_file}"
else
  echo "Ventoy USB-Stick ist nicht eingehangen oder die Prüsumme des Linux Mint Abbildes stimmt nicht!"
fi
		ende
		;;

    esac
    ;;


################################################################################
##  Hauptmenü => Linux Distributionen => Manjaro Linux
################################################################################

    ml|ML)
linux_dist_name="Manjaro Linux"
manjarolinux_iso_folder="/home/${USER}/"

	banner "Hauptmenü => Linux Distributionen => ${linux_dist_name}"

echo "  ${gl__format_red}| U0 |${gl__format_off}  ${linux_dist_name} Webseite öffnen"
echo "  ${gl__format_red}| U1 |${gl__format_off}  Offizielle ${linux_dist_name} Dokumentation öffnen"
echo ""
echo "  ${gl__format_red}| K0 |${gl__format_off}  KDE Plasma herunterladen"
echo "  ${gl__format_red}| K1 |${gl__format_off}  KDE Plasma auf (zB) einen USB-Stick schreiben"

glf__please_choose

    read manjarolinux

        case ${manjarolinux} in

		u0|U0)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => Offizielle Website öffnen"

xdg-open https://manjaro.org

		;;

		u1|U1)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => Offizielle ${linux_dist_name} Dokumentation öffnen"

xdg-open https://wiki.manjaro.org/index.php/Main_Page

		;;



################################################################################
##  Hauptmenü => Linux Distributionen => Manjaro Linux => KDE
################################################################################

		k0|k0)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => KDE Plasma herunterladen"

temp=$(mktemp ManjaroLinuxKDEdownloadXXX --suffix=.temp)
flavour="kde"
checksum_bin="/usr/bin/sha256sum"
iso_url="$(wget -q https://manjaro.org/products/download/x86 -O ${temp} ; sed 's/\.iso/.iso\n/g' x86 | grep -o "https.*kde.*iso$")"
iso_checksum_url="${iso_url}.sha256"
iso_file="$(echo ${iso_url} | grep -o "manjaro-kde.*iso")"
iso_checksum_file="$(echo ${iso_checksum_url} | grep -o "manjaro-kde.*sha256")"

[[ -f "${manjarolinux_iso_folder}${iso_file}" ]] || wget "${iso_url}" -P "${manjarolinux_iso_folder}"
[[ -f "${manjarolinux_iso_folder}${iso_checksum_file}" ]] || wget "${iso_checksum_url}" -P "${manjarolinux_iso_folder}" -O "${iso_checksum_file}"

checksum_image=$(${checksum_bin} "${manjarolinux_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${manjarolinux_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

    banner "Hauptmenü => Linux Distributionen => Manjaro Linux => KDE Plasma herunterladen"

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Installationsmedium (${iso_file}) ist korrekt."
else
  echo "Irgendetwas stimmt mit der Prüfsumme nicht!"
fi

		ende
		;;

		k1|K1)
		banner "Hauptmenü => Linux Distributionen => ${linux_dist_name} => KDE Plasma auf USB-Stick schreiben"

flavour="kde"
checksum_bin="/usr/bin/sha256sum"
iso_file="$(/bin/ls -1t ${manjarolinux_iso_folder} | grep "manjaro-kde.*iso$")"
iso_checksum_file="$(/bin/ls -1t ${manjarolinux_iso_folder} | grep "manjaro-kde.*iso.sha256$")"

checksum_image=$(${checksum_bin} "${manjarolinux_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${manjarolinux_iso_folder}${iso_checksum_file}" | grep -o "[a-z0-9]\{64\}")

if test -f "${manjarolinux_iso_folder}${iso_file}"
then
  echo "Datei existiert"
else
  echo "Datei existiert nicht"
fi

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
  echo ""
  echo "ACHTUNG: Alle Daten auf dem Zieldatneträger gehen verloren, wenn du jetzt weitermachst!"
  echo ""
  lsblk
  read -p "${format_red}Zielspeicher (z.B.: /dev/sdx): ${format_off}" device
  sudo dd if="${iso_folder}${iso_file}" of=${device} bs=4M status=progress && sync
else
  echo "Irgendwas ist schief gelaufen"
fi

		ende
		;;

    esac
    ;;



################################################################################
##  Hauptmenü => Linux Distributionen => openSUSE
################################################################################


	os|OS)
opensuse_iso_folder="${HOME}/"
		banner "Hauptmenü => Linux Distributionen => openSUSE"

echo "  ${gl__format_red}| S0 |${gl__format_off}  openSUSE Webseite öffnen"
echo ""
echo "  ${gl__format_red}| S1 |${gl__format_off}  Tumbleweed KDE Live-System herunterladen"
echo "  ${gl__format_red}| S2 |${gl__format_off}  Tumbleweed NET Install herunterladen"

glf__please_choose

    read opensuse

    case ${opensuse} in

		s0|S0)
		banner "Hauptmenü => Linux Distributionen => openSUSE => Offizielle Website öffnen"

xdg-open https://opensuse.org

		;;

		s1|S1)
		banner "Hauptmenü => Linux Distributionen => openSUSE => Tumbleweed KDE Live-System herunterladen"

checksum_bin="/usr/bin/sha256sum"
iso_url="https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-KDE-Live-x86_64-Current.iso"
iso_url_checksum="https://ftp.uni-erlangen.de/opensuse/tumbleweed/iso/openSUSE-Tumbleweed-KDE-Live-x86_64-Current.iso.sha256"
iso_file="openSUSE-Tumbleweed-KDE-Live-x86_64-Current.iso"
iso_file_checksum="openSUSE-Tumbleweed-KDE-Live-x86_64-Current.iso.sha256"

[[ -f "${opensuse_iso_folder}${iso_file}" ]] || wget "${iso_url}" -P "${opensuse_iso_folder}"
[[ -f "${opensuse_iso_folder}${iso_file_checksum}" ]] || wget "${iso_url_checksum}" -P "${opensuse_iso_folder}" -O "${iso_file_checksum}"

checksum_image=$(${checksum_bin} "${opensuse_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${opensuse_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
else
  echo "Irgendetwas stimmt mit der Prüfsumme nicht!"
fi

		ende
		;;

		s2|S2)
		banner "Hauptmenü => Linux Distributionen => openSUSE => Tumbleweed NET Install herunterladen"

checksum_bin="/usr/bin/sha256sum"
iso_url="https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-NET-x86_64-Current.iso"
iso_url_checksum="https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-NET-x86_64-Current.iso.sha256"
iso_file="openSUSE-Tumbleweed-NET-x86_64-Current.iso"
iso_file_checksum="openSUSE-Tumbleweed-NET-x86_64-Current.iso.sha256"

[[ -f "${opensuse_iso_folder}${iso_file}" ]] || wget "${iso_url}" -P "${opensuse_iso_folder}"
[[ -f "${opensuse_iso_folder}${iso_file_checksum}" ]] || wget "${iso_url_checksum}" -P "${opensuse_iso_folder}" -O "${iso_file_checksum}"

checksum_image=$(${checksum_bin} "${opensuse_iso_folder}${iso_file}" | awk '{print $1}')
checksum_chksumfile=$(grep "${flavour}" "${opensuse_iso_folder}${iso_file}.sha256" | grep -o "[a-z0-9]\{64\}")

if [[ "${checksum_image}" == "${checksum_chksumfile}" ]]
then
  echo "Die Prüfsumme des Abbildes ist korrekt."
else
  echo "Irgendetwas stimmt mit der Prüfsumme nicht!"
fi

		ende
		;;

    esac
    ;;

esac
;;



################################################################################
##  WLAN ein-/ausschalten
################################################################################

w|W)
banner "Hauptmenü => WLAN ein-/ausschalten"

if echo "${gl__wlanactive}" | grep "nicht verfügbar" &> /dev/null ; then
  bannertitel="Hauptmenü => WLAN ein-/ausschalten => WLAN wird eingeschaltet"
  banner
  echo -e "WLAN ist ausgeschaltet und wird jetzt eingeschaltet. Das kann ein paar Sekunden dauern: \n"
  nmcli radio wifi on
  sleep 5
  nmcli device status | grep "DEVICE\|wifi"
else
  bannertitel="Hauptmenü => WLAN ein-/ausschalten => WLAN wird ausgeschaltet"
  banner
  echo -e "WLAN ist eingeschaltet und wird jetzt ausgeschaltet. Das kann ein paar Sekunden dauern: \n"
  nmcli radio wifi off
  sleep 5
  nmcli device status | grep "DEVICE\|wifi"
fi

ende
;;



########################################################################
##  TON an/aus/leiser/lauter
########################################################################

b|B)
gl__lautstaerke=$(amixer | grep -o "\[off\]\|\[on\]" | head -1 | sed "s/\[//g" | sed "s/\]//g")
if [ ${gl__lautstaerke} = "off" ] ; then
  amixer sset Master unmute
else
  amixer sset Master mute
fi
export gl__lautstaerke
;;

n|N)
amixer set Master 10%+
;;

v|V)
amixer set Master 10%-
;;



################################################################################
##  Mikrofone ein-/ausschalten
################################################################################

m|M)
#amixer set Capture toggle
pactl set-source-mute @DEFAULT_SOURCE@ toggle
;;



################################################################################
##  Herunterfahren/Neustarten
################################################################################

r|R)
[[ -f /usr/sbin/shutdown ]] && shutdown_bin="/usr/sbin/shutdown"
[[ -f /usr/bin/shutdown ]]  && shutdown_bin="/usr/bin/shutdown"

banner "Hauptmenü => Herunterfahren/Neustarten"

echo "  ${gl__format_red}HERUNTERFAHREN${gl__format_off}"
echo "  ${gl__format_red}==============${gl__format_off}"
echo "  ${gl__format_red}| H1 |${gl__format_off}  Sofort herunterfahren"
echo "  ${gl__format_red}| H2 |${gl__format_off}  In x Minuten herunterfahren"
echo "  ${gl__format_red}| H3 |${gl__format_off}  Um exakt xx:xx Uhr herunterfahren"
echo ""
echo "  ${gl__format_red}NEUSTARTEN${gl__format_off}"
echo "  ${gl__format_red}==========${gl__format_off}"
echo "  ${gl__format_red}| N1 |${gl__format_off}  Sofort neustarten"
echo "  ${gl__format_red}| N2 |${gl__format_off}  In x Minuten neustarten"
echo "  ${gl__format_red}| N3 |${gl__format_off}  Um exakt xx:xx Uhr neustarten"
echo ""
echo "  ${gl__format_red}| C |${gl__format_off}  Herunterfahren/Neustarten abbrechen"

glf__please_choose

read shutdownreboot

    case ${shutdownreboot} in

	h1|H1)
	banner "Hauptmenü => Sofort Herunterfahren"

${shutdown_bin} -P now

	;;


	h2|H2)
	banner "Hauptmenü => In x Minuten Herunterfahren"

read -p "${gl__format_red}Bitte eine Ganzzahl eingeben (60 oder 65 oder ...) und ENTER drücken: ${gl__format_off}" minuten

${shutdown_bin} -h ${minuten}

	ende
	;;


	h3|H3)
	banner "Hauptmenü => Um exakt xx:xx Herunterfahren"

read -p "${gl__format_red}Bitte eine Ganzzahl eingeben (20:31 oder 23:59 oder...) und ENTER drücken: ${gl__format_off}" ssmm

${shutdown_bin} -h ${ssmm}

	ende
	;;


	n1|N1)
	banner "Hauptmenü => Sofort Neustarten"

${shutdown_bin} --reboot now

	;;


	n2|N2)
	banner "Hauptmenü => In x Minuten Neustarten"

read -p "${gl__format_red}Bitte eine Ganzzahl eingeben (60 oder 65 oder ...) und ENTER drücken: ${gl__format_off}" minuten

${shutdown_bin} -r +${minuten}

	ende
	;;


	n3|N3)
	banner "Hauptmenü => Um exakt xx:xx Neustarten"

read -p "${gl__format_red}Bitte eine Ganzzahl eingeben (20:31 oder 23:59 oder...) und ENTER drücken: ${gl__format_off}" ssmm

${shutdown_bin} -r ${ssmm}

	ende
	;;
	

	c|C)
	banner "Hauptmenü => Das geplante Herunterfahren/Neustarten abbrechen"

${shutdown_bin} -c

	ende
	;;

    esac

;;



################################################################################
##  bHELPER aktualisieren
################################################################################

u|U)
banner "Hauptmenü => bHELPER aktualisieren"

wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/bHELPER.sh -O ~/.bHELPER/bHELPER.sh
wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/bHELPER.png -O ~/.bHELPER/bHELPER.png
wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/README.md -O ~/.bHELPER/README.md

ende
;;



################################################################################
##  Debug
################################################################################

debug)
banner "Hauptmenü => Debug"

bash

;;



################################################################################
##  Einstellungen
################################################################################

e|E)
[[ -d "${gl__config_folder}" ]] || mkdir -p "${gl__config_folder}"
[[ -f "${gl__config_file}" ]] || touch "${gl__config_file}"

banner "Hauptmenü => Einstellungen"

unset desktopiconautom_status
grep -q "desktopiconautom=no" "${gl__config_file}" && desktopiconautom_status="$(echo "${gl__format_red}[ausgeschaltet]${gl__format_off}")"
grep -q "desktopiconautom=no" "${gl__config_file}" || desktopiconautom_status="$(echo "${gl__format_green}[eingeschaltet]${gl__format_off}")"
echo "  ${gl__format_red}| 0 |${gl__format_off}  Automatische Erstellung des Desktop Icons     ${desktopiconautom_status}"

echo "  ${gl__format_red}| E |${gl__format_off}  Dieses Skript editieren"

glf__please_choose

read einstellungen

    case ${einstellungen} in

    0)
    banner "Hauptmenü => Einstellungen => Automatisches Erstellung des Desktop Icons"

if [[ "${desktopiconautom_status}" =~ .*eingeschaltet.* ]] ; then
grep -q "desktopiconautom=" "${gl__config_file}" &>/dev/null || echo "desktopiconautom=no" >> "${gl__config_file}"
sed -i "s/^desktopiconautom=.*$/desktopiconautom=no/" "${gl__config_file}"
fi

if [[ "${desktopiconautom_status}" =~ .*ausgeschaltet.* ]] ; then
grep -q "desktopiconautom=" "${gl__config_file}" &>/dev/null || echo "desktopiconautom=yes" >> "${gl__config_file}"
sed -i "s/^desktopiconautom=.*$/desktopiconautom=yes/" "${gl__config_file}"
fi

	banner "Hauptmenü => Einstellungen => Automatisches Erstellung des Desktop Icons"
grep -q "desktopiconautom=no" "${gl__config_file}" &>/dev/null && echo "Die Automatische Erstellung des Desktop Icons wurde ${gl__format_red}[ausgeschaltet]${gl__format_off}" 
grep -q "desktopiconautom=yes" "${gl__config_file}" &>/dev/null && echo "Die Automatische Erstellung des Desktop Icons wurde ${gl__format_green}[eingeschaltet]${gl__format_off}"
    ende
    ;;

	e|E)
rm "${0}".dasi10                2> /dev/null
cp "${0}".dasi09 "${0}".dasi10  2> /dev/null
cp "${0}".dasi08 "${0}".dasi09  2> /dev/null
cp "${0}".dasi07 "${0}".dasi08  2> /dev/null
cp "${0}".dasi06 "${0}".dasi07  2> /dev/null
cp "${0}".dasi05 "${0}".dasi06  2> /dev/null
cp "${0}".dasi04 "${0}".dasi05  2> /dev/null
cp "${0}".dasi03 "${0}".dasi04  2> /dev/null
cp "${0}".dasi02 "${0}".dasi03  2> /dev/null
cp "${0}".dasi01 "${0}".dasi02  2> /dev/null
cp "${0}"        "${0}".dasi01  2> /dev/null
nohup ${gl__fav_editor} "${0}" &
	;;

    esac

;;

esac

. "${gl__SkriptPfadName}" && exit 0
