#!/usr/bin/env bash
# Version: 2022-09-16 00:35

[[ -d ${HOME}/.bHELPER/ ]] || mkdir ~/.bHELPER
wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/bHELPER.sh -O ~/.bHELPER/bHELPER.sh
wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/bHELPER.png -O ~/.bHELPER/bHELPER.png
