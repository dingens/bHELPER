# bHELPER

Der Grund für dieses Skript ist, dass ich immer mit Leuten die Probleme an ihrem Linux Rechner hatten sagen musste "Gib mal das ein ..." um an Informationen zu kommen. bHELPER.sh soll hier Abhilfe schaffen indem Leute sich das Skript installieren und mir einfach per copy&paste das Ergebnis schicken was ich brauche.
Dann kam noch der Grund hinzu, daß das Skript eine Art schweizer Taschenmesser im Alltag sein kann.

## (Neu-)Installation

Ihr müsst erst einmal ein Terminal öffnen. Das geht bei (fast) jedem Linux mit `Strg`+`Alt`+`T` oder in dem Programmmenü eurer Linux Distribution nach "Terminal" suchen. Dann den folgenden Inhalt des Codeblock dort einfügen:

```
[[ -d ${HOME}/.bHELPER/ ]] || mkdir ~/.bHELPER ; wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/bHELPER.sh -O ~/.bHELPER/bHELPER.sh ; wget https://git.disroot.org/dingens/bHELPER/raw/branch/master/bHELPER.png -O ~/.bHELPER/bHELPER.png ; bash ${HOME}/.bHELPER/bHELPER.sh
```

## Das erste mal Starten

### Via Icon auf dem Desktop

Nach dem ersten Starten wird auf eurer Arbeitsfläche (Desktop) ein Icon angelegt. Darauf geklickt startet bHELPER.

### Via Terminal

Ihr könnt es auch direkt vom Terminal aus starten. Dazu ein Terminal öffnen mit `Strg`+`Alt`+`T` und dann folgenden Code dort einfügen:

```
bash ~/.bHELPER/bHELPER.sh
```
